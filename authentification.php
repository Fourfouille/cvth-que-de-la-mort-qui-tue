<?php

require_once 'modeles/connexion.php';

$connexion = new Connexion();
$utilisateur = $connexion->getAllMembre();

while($admin = $utilisateur->fetch())
{
    if(($_POST['login'] == $admin['username'] OR $_POST['login'] == $admin['email']) AND password_verify($_POST['password'], $admin['motDePasse']))
    {    
        session_start();
        $_SESSION['login'] = $admin['idPersonne'];
        $_SESSION['type'] = $admin['role'];

        header('Location: ./');
    } else {
        $erreur="Nom d'utilisateur ou mot de passe incorrect";
    }
}
require 'vues/connexion.php';
require 'vues/gabarit.php';

