-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 04 juin 2020 à 14:38
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cvtheque`
--
CREATE DATABASE IF NOT EXISTS `cvtheque` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cvtheque`;

-- --------------------------------------------------------

--
-- Structure de la table `competence`
--

CREATE TABLE `competence` (
  `idCompetence` int(11) NOT NULL,
  `idCv` int(11) NOT NULL,
  `libelleCompetence` varchar(255) CHARACTER SET utf8 NOT NULL,
  `descriptionCompetence` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `cv`
--

CREATE TABLE `cv` (
  `idCv` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `poste` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `age` int(3) NOT NULL,
  `pays` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `presentation` varchar(255) NOT NULL,
  `jeux1` varchar(255) NOT NULL,
  `jeux2` varchar(255) NOT NULL,
  `jeux3` varchar(255) NOT NULL,
  `photoProfil` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `telephone` int(15) NOT NULL,
  `disponibilite` varchar(255) NOT NULL,
  `statut` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cv`
--

INSERT INTO `cv` (`idCv`, `nom`, `prenom`, `poste`, `email`, `age`, `pays`, `region`, `presentation`, `jeux1`, `jeux2`, `jeux3`, `photoProfil`, `mail`, `telephone`, `disponibilite`, `statut`) VALUES
(1, 'fournier', 'anthony', 'webdesign', 'fournier@anthony.fr', 42, 'Roumanie', 'sud', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin leo lectus, consequat a mattis in, aliquam a velit. ', 'wow', 'warzone', 'csgo', 'http://localhost/cvth-que/img/cat.png', '', 0, 'libre', 1),
(2, 'Naveau', 'Melanie', 'projet', 'melanie@naveau.fr', 20, 'France', 'bretagne', 'Pellentesque ut porta dui. Phasellus iaculis nisi quam, vitae mollis mi facilisis a.', 'warzone', 'valorant', '', 'http://localhost/cvth-que/img/tucker.png', '', 0, 'equipe', 0);

-- --------------------------------------------------------

--
-- Structure de la table `domaine`
--

CREATE TABLE `domaine` (
  `idDomaine` int(11) NOT NULL,
  `libelleDomaine` varchar(255) CHARACTER SET utf8 NOT NULL,
  `idCompetence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `experience`
--

CREATE TABLE `experience` (
  `idExperience` int(11) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `nomExperience` varchar(150) NOT NULL,
  `idCv` int(11) NOT NULL,
  `structure` varchar(255) NOT NULL,
  `descriptionExperience` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `recompense`
--

CREATE TABLE `recompense` (
  `idRecompense` int(11) NOT NULL,
  `idCv` int(11) NOT NULL,
  `nomRecompense` varchar(50) CHARACTER SET utf8 NOT NULL,
  `dateRecompense` date NOT NULL,
  `nomJeu` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idPersonne` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `motDePasse` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idPersonne`, `username`, `email`, `role`, `motDePasse`) VALUES
(1, 'joueur', 'joueur@clutch.fr', 1, '$2y$10$TkwcAWgEoXlreoC8LeR2SeYDYtHBiMMSSqofHBnK23Wk5FOvAJ2I6'),
(2, 'admin', 'admin@clutch.fr', 2, '$2y$10$fKPDXfZPHbdP/L7otW1lgO7a84uXFJMpMqeQIA/AKCMljyzZQ/v9G');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `competence`
--
ALTER TABLE `competence`
  ADD PRIMARY KEY (`idCompetence`);

--
-- Index pour la table `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`idCv`);

--
-- Index pour la table `domaine`
--
ALTER TABLE `domaine`
  ADD PRIMARY KEY (`idDomaine`);

--
-- Index pour la table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`idExperience`);

--
-- Index pour la table `recompense`
--
ALTER TABLE `recompense`
  ADD PRIMARY KEY (`idRecompense`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idPersonne`),
  ADD UNIQUE KEY `username` (`username`,`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `competence`
--
ALTER TABLE `competence`
  MODIFY `idCompetence` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `cv`
--
ALTER TABLE `cv`
  MODIFY `idCv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `domaine`
--
ALTER TABLE `domaine`
  MODIFY `idDomaine` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `experience`
--
ALTER TABLE `experience`
  MODIFY `idExperience` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `recompense`
--
ALTER TABLE `recompense`
  MODIFY `idRecompense` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idPersonne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
