<?php

require_once 'modeles/connexion.php';

$connexion = new Connexion();
$utilisateur = $connexion->getAllMembre();

while($admin = $utilisateur->fetch())
{
    if($_POST['identifiant'] == $admin['username'])
    {    
        $erreur="Nom d'utilisateur déjà utilisé";
    } elseif ($_POST['email'] == $admin['email']) 
    {
        $erreur="Email déjà utilisé";
    } else {

        $connexion->addMembre();
        echo "inscrit";
        header('Location: ./');
    } 
}
require 'vues/inscription.php';
require 'vues/gabarit.php';

