//Search form
$(document).ready(function(){
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".liste").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

//search in filters
$(document).ready(function(){
    $("#pays").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".dropdown-menu li").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

//Checkbox
// // Cache table element
// var $tableElement = $('.js-table');

// // Init dataTable plugin
// var dataTable = $tableElement.DataTable({
// 	"paging":   false,
// 	"tDom": '<"top">rt<"bottom"><"clear">' // removed all from table structure
// });

// // Cache inputs wrapper
// var $inputWrapper = $('.input-wrapper');

// // 0 based column numbers
// var columnsToSort = $tableElement.attr('data-sort-by');

// // if columnsToSort not provided or empty - sort by all
// columnsToSort = (columnsToSort) ? $tableElement.attr('data-sort-by').split(',') : 'all';

// // get all columns count
// var columnsCount = dataTable.columns()[0].length;

// // loop iterations based on columnsToSort existance
// var columnsToSortCount = (columnsToSort === 'all') ? columnsCount : columnsToSort.length;


// for(var k = 0; k < columnsToSortCount; k++) {

// 	var sortBy = (columnsToSort === 'all') ? k : parseInt(columnsToSort[k], 10);

// 	// Check if sortBy is in column count range
// 	if(sortBy >= 0 && sortBy <= columnsCount-1) {
// 		// get unique column names
// 		var sortValues = uniqueStringArray(dataTable.column(sortBy).data()); 
// 	}

// 	// Join all input-label pairs and inject into DOM
// 	// its own wrapper
// 	$localInputsWrapper = $('<div />', {
// 		'class': 'js-input-group inputs-group',
// 		html: genInputs(sortValues, sortBy)
// 	});

// 	$inputWrapper.append($localInputsWrapper);
// }


// $inputWrapper.on('change','.js-filter-checkbox', function (e) {
// 	// Column link
// 	var columnLink = parseInt($(this).attr('data-column-link'));

// 	// filter text
// 	var filter = genFilterByChecked($inputWrapper.find('.js-filter-checkbox:checked[data-column-link='+columnLink+']'));

// 	// Inject filter
// 	dataTable
// 		.column(columnLink)
// 		.search(filter, true, false)
// 		.draw();
// });

// // Create label-input pairs from array
// function genInputs(arr, column) {
// 	// All inputs cache array
// 	var inputs = [];
// 	for(var i = 0; i < arr.length; i++){

// 		// Store current item
// 		var item = arr[i];

// 		item = item.charAt(0).toUpperCase() + item.slice(1);

// 		// compute web-safe id
// 		var id = encodeURIComponent(item)+i;

// 		// create label-input pairs
// 		inputs.push('<label for="'+id+'"><input type="checkbox" class="js-filter-checkbox" data-column-link="'+column+'" id="'+id+'" value="'+item+'"/> '+item+'</label>');
// 	};
// 	// Join all input-label pairs and inject into DOM
// 	return inputs.join('\n');
// }

// // Build filter based on checked inputs
// function genFilterByChecked($items) {
// 	var filter ='';
// 	$items.each(function(){
// 		filter += "|" + this.value;
// 	})

// 	// remove first "|"
// 	return filter.substring(1);
// }

// // Uniquify array without case tracking
// function uniqueStringArray(xs) {
// 	var seen = {}
// 	return xs.filter(function(x) {
// 		if (seen[x.toLowerCase()]) {
// 			return;
// 		}

// 		seen[x.toLowerCase()] = true;
// 		return x;
// 	})
// }

//FIltres sur colonne
// function mounted() {
//     $('#table').bootstrapTable()
// }

//Checkboxes
// $(function () {
//     $('#table').bootstrapTable({
//         data: data
//     });
//     $('#table').on('check.bs.table', function (e, row, $el) {
//     	alert('check index: ' + $el.closest('tr').data('index'));
//     });
//     $('#table').on('uncheck.bs.table', function (e, row, $el) {
//     	alert('uncheck index: ' + $el.closest('tr').data('index'));
//     });
// });

 //multi-range slider
