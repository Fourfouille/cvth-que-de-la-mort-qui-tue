<?php
require('initBdd.php');

class Connexion extends Bdd
{
    public function getMembre()
    {    
        $membre = $this->initBdd()->query('SELECT * FROM utilisateur where role = 1');
        return $membre;
    }

    public function getAdmin()
    {
        $admin = $this->initBdd()->query("SELECT * from utilisateur where role = 2");
        return $admin;
    }

    public function getAllMembre()
    {    
        $membre = $this->initBdd()->query('SELECT * FROM utilisateur');
        return $membre;
    }

    public function addMembre()
    {
        $insert = $this->initBdd()->prepare("INSERT into utilisateur (username, email, role, motDePasse) values (:identifiant, :email, :role, :password)");

        $insert->execute(array('identifiant' => $_POST['identifiant'], 'email' => $_POST['email'], 'role' => 1 , 'password' => password_hash($_POST["password"], PASSWORD_DEFAULT)));
    }

}
