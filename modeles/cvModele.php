<?php
require('initBdd.php');

class cvModele extends Bdd
{
	public function insertCV()
	{
        $insertCv = $this->initBdd()->prepare("INSERT into cv values (:Nom, :Prenom, :Poste, :Email, :Age, :Pays, :Region, :Presentation, :Jeux1, :Jeux2, :Jeux3, :Photo, :Mail, :Telephone, :Dispo, :Statut)");

        $insertCv->execute(array('Nom' => $_POST['nom'], 'Prenom' => $_POST['prenom'], 'Poste' => $_POST['poste'], 'Email' => $_POST['email'], 'Age' => $_POST['age'], 'Pays' => $_POST['pays'], 'Region' => $_POST['region'], 'Presentation' => $_POST['presentation'], 'Jeux1' => $_POST['jeux1'], 'Jeux2' => $_POST['jeux2'], 'Jeux3' => $_POST['jeux3'], 'Photo' => $_POST['photo'], 'Mail' => $_POST['email'], 'Telephone' => $_POST['telephone'], 'Dispo' => $_POST['disponibilite'], 'Statut' => $_POST['statut']));
    }

	public function supprimerCV($id)
	{
		$this->initBdd()->exec("DELETE from cv where idCV ='".$id."'");
    }
    
    public function getCv()
    {           
        $allCv = $this->initBdd()->query('SELECT * FROM cv');
        return $allCv;
    }

    public function getSingleCv($id)
	{
		$cv = $this->initBdd()->query("SELECT nom, prenom, poste, email, age, pays, region, presentation, jeux1, jeux2, jeux3, photoProfil, mail, telephone, disponibilite, statut from cv where idCv=".$id);
		return $cv;
	}
}
