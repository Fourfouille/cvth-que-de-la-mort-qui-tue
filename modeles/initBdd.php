<?php 

abstract class Bdd 
{
    private $bdd;

    public function initBdd()
    {
        try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=cvtheque;charset=utf8', 'root', 'root'); //Connexion à la bdd
            $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (Exception $e) //$e erreur
        {
                die('Erreur : ' . $e->getMessage()); //getMessage récupère le message de l'erreur
        }
        return $bdd; 
    }
}

?>