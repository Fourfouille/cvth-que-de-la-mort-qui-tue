/**
 * Fichier de configuration pour les prefix CSS
 */

module.exports = {
	plugins: [
		require('autoprefixer'),
		require('cssnano')
	]
}