
<?php $title = 'CLUTCH - Tous les profils' ?>

<?php ob_start(); ?> <!-- va remplacer le $content dans la page gabarit -->

	<main role="main" class="inner cover">
		<div class="container-fluid">
            <div class="row">
                <div class="col-3">
                    <h2>Filtrez votre recherche !</h2>
                </div>
                <div class="col-7">
                <h2>Consultez les derniers CV postés</h2>
                </div>
            </div>
			<div class="row"> <!-- Filtres par pays -->
                <div class="col-3">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Pays</button>
                        <ul class="dropdown-menu">
                            <input class="form-control" id="pays" type="text" placeholder="Rechercher">
                            <li><a href="#">France</a></li>
                            <li><a href="#">Angleterre</a></li>
                            <li><a href="#">USA</a></li>
                            <li><a href="#">Allemagne</a></li>
                            <li><a href="#">Espagne</a></li>
                            <li><a href="#">Japon</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row"> 
                <div class="col-3">
                    <ul id="filters">	
                        <h3>Statut</h3>
                        <li>
                            <input type="checkbox" value="0" id="joueur" checked />
                            <label for="joueur">0</label>
                        </li>
                        <li>
                            <input type="checkbox" value="1" id="coach" checked />
                            <label for="coach">1</label>
                        </li>
                        <h3>Disponibilité</h3>
                        <li>
                            <input type="checkbox" value="libre" id="libre" checked/>
                            <label for="libre">Libre</label>
                        </li>
                        <li>
                            <input type="checkbox" value="equipe" id="equipe" checked/>
                            <label for="equipe">En équipe</label>
                        </li>
                        <h3>Jeux</h3>
                        <li>
                            <input type="checkbox" value="wow" id="wow" checked/>
                            <label for="wow">World of Warcraft</label>
                        </li>
                        <li>
                            <input type="checkbox" value="warzone" id="warzone" checked/>
                            <label for="warzone">Call of Duty Warzone</label>
                        </li>
                        <li>
                            <input type="checkbox" value="lol" id="lol" checked/>
                            <label for="lol">League of Legends</label>
                        </li>
                        <li>
                            <input type="checkbox" value="rainbow" id="rainbow" checked/>
                            <label for="rainbow">Rainbow Six Siege</label>
                        </li>
                        <li>
                            <input type="checkbox" value="csgo" id="csgo" checked/>
                            <label for="csgo">Counter Strike Go</label>
                        </li>
                        <li>
                            <input type="checkbox" value="bf" id="bf" checked/>
                            <label for="bf">Battlefield 5</label>
                        </li>
                        <li>
                            <input type="checkbox" value="valorant" id="valorant" checked/>
                            <label for="valorant">Valorant</label>
                        </li>
                        <li>
                            <input type="checkbox" value="hs" id="hs" checked/>
                            <label for="hs">Hearthstone</label>
                        </li>
                    </ul>            
                </div> 
                <!-- Affichage des profils -->
                <?php                    
                        while($donnees = $arrayCv->fetch())
                        {
                ?>		
                <a href="index.php?p=singleCv&a=<?php echo $donnees['idCv']?>" class="lienCv">
                    <div class="liste"> <!-- Enlever les br et mettre à la ligne en CSS -->
                        <img src="<?php echo $donnees['photoProfil'];?>" alt="Photo de profil" height="100px" style="border-radius:50%"><br>
                        <?php echo $donnees['nom'];?><br>
                        Pays: <?php echo $donnees['pays'];?><br>
                        Age: <?php echo $donnees['age'];?><br>
                        Poste: <?php echo $donnees['poste'];?><br>
                        Recherche: <div class="statut" data-value="<?= $donnees['statut'];?>"><?php echo $donnees['statut'];?></div><br>
                        Jeux 1: <div class="jeux1" data-value="<?= $donnees['jeux1'];?>"><?php echo $donnees['jeux1'];?></div><br>
                        Jeux 2: <div class="jeux2" data-value="<?= $donnees['jeux2'];?>"><?php echo $donnees['jeux2'];?></div><br>
                        Jeux 3: <div class="jeux3" data-value="<?= $donnees['jeux3'];?>"><?php echo $donnees['jeux3'];?></div><br>
                        Présentation: <?php echo $donnees['presentation'];?><br>
                        Disponibilité : <div class="dispo" data-value="<?= $donnees['disponibilite'];?>"><?php echo $donnees['disponibilite'];?></div><br>
                    </div>
                </a>
                <?php                
                        }
                    
                ?>
            </div>     
		</div>
	</main>	

<script type="text/javascript"> 
    document.addEventListener('DOMContentLoaded', () => { // Quand le DOM est chargé
        document.querySelectorAll('#filters input[type="checkbox"]').forEach(checkbox => { // On récupère les checkboxes contenues dans #filter et on boucle dessus
            checkbox.addEventListener('input', function() { // Pour chaque checkbox, on ajoute un événement input (plutôt que click car, le clique n'est pas la seul façon de cocher une checkbox)
                Array.from(document.querySelectorAll('.liste > .statut')) // On récupère tout les div.status contenu dans les div.liste et on les place dans un Array (plutôt que la NodeList que renvoi querySelectorAll) pour travailler avec filter et map par la suite (NodeList ne dispose pas de ces méthodes contrairement aux array)
                    .filter(statutDiv => statutDiv.dataset.value === checkbox.value) // On filtre les div.status ayant un attribut data-value égal à la valeur de la checkbox actuelle
                    .map(statutDiv => statutDiv.closest('.liste')) // On récupère le premier ancètre div.liste de chaque div.status
                    .forEach(listeDiv => { // Pour chaque div.liste
                        if(checkbox.checked) { // Si la checkbox actuelle est cochée
                            listeDiv.style.display = null; // On retire les style display et opacity des div.liste (pour qu'ils soient affichés)
                            listeDiv.style.opacity = null;
                        }
                        else { // Sinon
                            listeDiv.addEventListener('transitionend', () => listeDiv.style.display = 'none', {once: true}); // On ajoute un événement 'transitionend' déclenchable qu'une seule fois ({once: true}) qui ajoute la règle "display: none;" sur les div.liste
                            listeDiv.style.opacity = 0; // On définit l'opacité des div.liste à 0. Quand la transition css sera terminer, l'événement transitionend sera déclenché.
                        }
                    });
                    Array.from(document.querySelectorAll('.liste > .dispo')) // On récupère tout les div.status contenu dans les div.liste et on les place dans un Array (plutôt que la NodeList que renvoi querySelectorAll) pour travailler avec filter et map par la suite (NodeList ne dispose pas de ces méthodes contrairement aux array)
                    .filter(dispoDiv => dispoDiv.dataset.value === checkbox.value) // On filtre les div.status ayant un attribut data-value égal à la valeur de la checkbox actuelle
                    .map(dispoDiv => dispoDiv.closest('.liste')) // On récupère le premier ancètre div.liste de chaque div.status
                    .forEach(listeDiv => { // Pour chaque div.liste
                        if(checkbox.checked) { // Si la checkbox actuelle est cochée
                            listeDiv.style.display = null; // On retire les style display et opacity des div.liste (pour qu'ils soient affichés)
                            listeDiv.style.opacity = null;
                        }
                        else { // Sinon
                            listeDiv.addEventListener('transitionend', () => listeDiv.style.display = 'none', {once: true}); // On ajoute un événement 'transitionend' déclenchable qu'une seule fois ({once: true}) qui ajoute la règle "display: none;" sur les div.liste
                            listeDiv.style.opacity = 0; // On définit l'opacité des div.liste à 0. Quand la transition css sera terminer, l'événement transitionend sera déclenché.
                        }
                    });
                    Array.from(document.querySelectorAll('.liste > .jeux1')) // On récupère tout les div.status contenu dans les div.liste et on les place dans un Array (plutôt que la NodeList que renvoi querySelectorAll) pour travailler avec filter et map par la suite (NodeList ne dispose pas de ces méthodes contrairement aux array)
                    .filter(jeux1Div => jeux1Div.dataset.value === checkbox.value) // On filtre les div.status ayant un attribut data-value égal à la valeur de la checkbox actuelle
                    .map(jeux1Div => jeux1Div.closest('.liste')) // On récupère le premier ancètre div.liste de chaque div.status
                    .forEach(listeDiv => { // Pour chaque div.liste
                        if(checkbox.checked) { // Si la checkbox actuelle est cochée
                            listeDiv.style.display = null; // On retire les style display et opacity des div.liste (pour qu'ils soient affichés)
                            listeDiv.style.opacity = null;
                        }
                        else { // Sinon
                            listeDiv.addEventListener('transitionend', () => listeDiv.style.display = 'none', {once: true}); // On ajoute un événement 'transitionend' déclenchable qu'une seule fois ({once: true}) qui ajoute la règle "display: none;" sur les div.liste
                            listeDiv.style.opacity = 0; // On définit l'opacité des div.liste à 0. Quand la transition css sera terminer, l'événement transitionend sera déclenché.
                        }
                    });
                    Array.from(document.querySelectorAll('.liste > .jeux2')) // On récupère tout les div.status contenu dans les div.liste et on les place dans un Array (plutôt que la NodeList que renvoi querySelectorAll) pour travailler avec filter et map par la suite (NodeList ne dispose pas de ces méthodes contrairement aux array)
                    .filter(jeux2Div => jeux2Div.dataset.value === checkbox.value) // On filtre les div.status ayant un attribut data-value égal à la valeur de la checkbox actuelle
                    .map(jeux2Div => jeux2Div.closest('.liste')) // On récupère le premier ancètre div.liste de chaque div.status
                    .forEach(listeDiv => { // Pour chaque div.liste
                        if(checkbox.checked) { // Si la checkbox actuelle est cochée
                            listeDiv.style.display = null; // On retire les style display et opacity des div.liste (pour qu'ils soient affichés)
                            listeDiv.style.opacity = null;
                        }
                        else { // Sinon
                            listeDiv.addEventListener('transitionend', () => listeDiv.style.display = 'none', {once: true}); // On ajoute un événement 'transitionend' déclenchable qu'une seule fois ({once: true}) qui ajoute la règle "display: none;" sur les div.liste
                            listeDiv.style.opacity = 0; // On définit l'opacité des div.liste à 0. Quand la transition css sera terminer, l'événement transitionend sera déclenché.
                        }
                    });
                    Array.from(document.querySelectorAll('.liste > .jeux3')) // On récupère tout les div.status contenu dans les div.liste et on les place dans un Array (plutôt que la NodeList que renvoi querySelectorAll) pour travailler avec filter et map par la suite (NodeList ne dispose pas de ces méthodes contrairement aux array)
                    .filter(jeux3Div => jeux3Div.dataset.value === checkbox.value) // On filtre les div.status ayant un attribut data-value égal à la valeur de la checkbox actuelle
                    .map(jeux3Div => jeux3Div.closest('.liste')) // On récupère le premier ancètre div.liste de chaque div.status
                    .forEach(listeDiv => { // Pour chaque div.liste
                        if(checkbox.checked) { // Si la checkbox actuelle est cochée
                            listeDiv.style.display = null; // On retire les style display et opacity des div.liste (pour qu'ils soient affichés)
                            listeDiv.style.opacity = null;
                        }
                        else { // Sinon
                            listeDiv.addEventListener('transitionend', () => listeDiv.style.display = 'none', {once: true}); // On ajoute un événement 'transitionend' déclenchable qu'une seule fois ({once: true}) qui ajoute la règle "display: none;" sur les div.liste
                            listeDiv.style.opacity = 0; // On définit l'opacité des div.liste à 0. Quand la transition css sera terminer, l'événement transitionend sera déclenché.
                        }
                    });
            });
        });
    }); 
    
</script>

<?php $content = ob_get_clean(); ?>




