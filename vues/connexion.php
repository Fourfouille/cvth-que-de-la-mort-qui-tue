
<?php

$titre = "Connexion";

ob_start(); 
?>

<div class="formulaire well">
    <div id="formContent">    
        <div id="inscription">
            <a class="underlineHover" href="inscription.php">Pas encore inscrit ?</a>
        </div>
        <form action="authentification.php" method="post">
            <input type="text" id="login" class="fadeIn second" name="login" placeholder="login">
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
            <input type="submit" class="fadeIn fourth" value="Log In">
        </form>
        <?php 
        if (!empty($erreur)){
            echo "<p style=\"color:red\">". $erreur ."</p>"; /*Il faudra passer le style dans un fichier css*/
        }
        ?>
        <div id="formFooter">
            <a class="underlineHover" href="#">Mot de passe oublié ?</a>
        </div>
</div>
<?php $content = ob_get_clean(); ?>