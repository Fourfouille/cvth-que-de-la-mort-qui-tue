<?php $title = 'PLAY - Remplir son profil' ?>

<?php ob_start(); ?>
	<main role="main" class="inner cover">
		<div class="container-fluid">
			<div class="row">
                <form name="creationCv" method="post" action="form_envoi.php">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="photoProfil">Photo de profil</label>
                            <input type="file" class="form-control-file" id="photoProfilFichier">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="nom">Nom *</label>
                            <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="prenom">Prénom *</label>
                            <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prénom" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="age">Age</label>
                            <input type="number" class="form-control" id="age" name="age" placeholder="Age">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="poste">Poste *</label>
                            <select id="poste" name="poste" class="form-control">
                                <option selected>Choisir...</option>
                                <option>Manager</option>
                                <option>Joueur</option>
                                <option>Sponsor</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="pays">Pays *</label>
                            <select id="pays" class="form-control">
                                <option selected>Choisir...</option>
                                <option>France</option>
                                <option>Roumanie</option>
                                <option>Angleterre</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="region">Région *</label>
                            <select id="region" class="form-control">
                                <option selected>Choisir...</option>
                                <option>Normandie</option>
                                <option>Bretagne</option>
                                <option>Paris</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="presentation">Présentation *</label>
                        <textarea class="form-control" id="presentation" rows="4"></textarea>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="jeu1">Jeu 1 *</label>
                            <select id="jeu1" class="form-control">
                                <option selected>Choisir...</option>
                                <option>Wow</option>
                                <option>Lol</option>
                                <option>Trackmania</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="jeu2">Jeu 2 </label>
                            <select id="jeu2" class="form-control">
                                <option selected>Choisir...</option>
                                <option>Wow</option>
                                <option>Lol</option>
                                <option>Trackmania</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="jeu3">Jeu 3 </label>
                            <select id="jeu3" class="form-control">
                                <option selected>Choisir...</option>
                                <option>Wow</option>
                                <option>Lol</option>
                                <option>Trackmania</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="experience">Expérience </label>
                            <input type="text" class="form-control" id="experience" placeholder="Nom de l'experience">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="dateDebut">Date de début</label>
                            <input type="date" class="form-control" id="dateDebut">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="dateFin">Date de fin</label>
                            <input type="date" class="form-control" id="dateFin">
                        </div>
                    </div>
                    <button>Ajouter une expérience</button>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="domaine">Domaine *</label>
                            <select id="domaine" class="form-control">
                                <option selected>Choisir...</option>
                                <option>Stream</option>
                                <option>FPS</option>
                                <option>TPS</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="competence">Compétence</label>
                            <input type="text" class="form-control" id="competence" placeholder="competence">
                        </div>
                        <button>Ajouter une compétence</button>
                    </div>
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                </form>
			</div>
		</div>
	</main>	
    

<?php $content = ob_get_clean(); ?>

<?php require('gabarit.php'); ?>