<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?></title>
    <link rel="icon" sizes="32x32" href="assets/images/favicon-32x32.png" type="image/png">
	<link rel="stylesheet" href="public/dist/main.css">
	<!--<link href="http://localhost/cvth-que/css/style.css" rel="stylesheet"> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>
	<script src="js/script.js"></script>
    <script src="public/dist/main.js" defer></script>
    <script src="assets/js/app.js"></script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<!-- <script src="../ckeditor/ckeditor.js"></script> -->

</head>
<body>
<div class="container">
<?php if(isset($_SESSION['login'])) {?>    
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php?p=accueil"><img src="img/CLUTCH.png" alt="Logo clutch"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <!-- Search form -->
                <form class="form-inline d-flex flex-row-reverse md-form form-sm mt-0">
                    <i class="fas fa-search" aria-hidden="true"></i>
                    <input id="search" class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Rechercher"
                    aria-label="Search">
                </form>
                <ul class="navbar-nav navbar-right">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php?p=accueil">Accueil<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?p=modification">Modifier mon CV</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Administration</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="deconnexion.php">Me déconnecter</a>
                </li>
                </ul>
            </div>
        </nav>
    </header>
<?php } else { ?>
    <header>
            <nav>
                <div class="nav-left">
                    <a href="#"><img alt="" src="assets/images/logo.png"></a>
                    <p class="title">CLUTCH</p>
                </div>
                <div class="nav-right">
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Nous contacter</a></li>
                        <li><button type="button" class="border-button modal-login">Se connecter</button></li>
                        <li><button type="button" class="bg-active full-button modal-signin">S'inscrire</button></li>
                    </ul>
                </div>
            </nav>
        </header>
<?php } ?>

	<?= $content ?> <!-- appel du contenu -->
	<div class="footer">
            <div class="flex-footer">
                <div class="first clip">
                    <p>de 1500 <br> CV en ligne</p>
                </div>
                <div class="second clip">
                <p>de 200 <br>contacts par jour</p>
                </div>
                <div class="third clip">
                    <p>de 100 <br>équipes créées</p>   
                </div>
            </div>
        </div>
    </div>
 
</body>
</html>
