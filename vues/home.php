<?php $title = 'CLUTCH - CVthèque gaming' ?>

<?php ob_start(); ?>

<div class="content-left">
    <img src="assets/images/fille_pageconnexion1.png" alt="">
</div>
<div class="content-right">
    <h1>Ciblez et approchez les meilleurs talents <br> dont vous avez besoin !</h1>
    <p>Visualisez les CV et contactez facilement les joueurs afin de créer ou améliorer votre équipe selon vos critères.<br><br> Dîtes adieu à vos soirées en solo et aux défaites à répétition !</p><br>
    <form action="#" method="get">
        <button class="sign-up-button bg-active">S'inscrire !</button>
    </form> 
</div>

<div class="modal-bg">
    <div class="modal">
        <div class="modal-left">
            <h2>Inscrivez-vous <br>pour trouvez vos futurs teammates !</h2>
            <input type="text" placeholder="Pseudo">
            <input type="email" placeholder="Email">
            <input type="email" placeholder="Confirmation d'email">
            <input type="password" placeholder="Mot de passe">
            <button class="full-button">S'inscrire</button>
        </div>
        <div class="modal-right">
            <img src="assets/images/mecsavecfond2.png" alt="">
            <span class="modal-close">X</span>
        </div>
    </div>
</div>

    <div class="modal-bg-2">
        <div class="modal-2">
            <div class="modal-left">
                <h2>Connectez vous !</h2>
                <input type="text" placeholder="Pseudo ou email">
                <input type="password" placeholder="Mot de passe">
                <button class="full-button">Se connecter</button>
            </div>
            <div class="modal-right">
                <img src="assets/images/mecsavecfond2.png" alt="">
                <span class="modal-close-2">X</span>
            </div>
        </div>
    </div>
    

<?php $content = ob_get_clean(); ?>
