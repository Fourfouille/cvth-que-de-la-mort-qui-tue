
<?php

$titre = "Inscription";

ob_start(); 
?>

<div class="formulaire well">
    <div id="formContent">    
        <div id="inscription">
            <a class="underlineHover" href="connexion.php">Déjà inscrit ?</a>
        </div>
        <form action="inscrit.php" method="post">
            <input type="text" id="identifiant" class="fadeIn second" name="identifiant" placeholder="identifiant">
            <input type="email" id="email" class="fadeIn second" name="email" placeholder="email">
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
            <input type="submit" class="fadeIn fourth" value="S'inscrire">
        </form>
        <?php 
        if (!empty($erreur)){
            echo "<p style=\"color:red\">". $erreur ."</p>"; /*Il faudra passer le style dans un fichier css*/
        }
        ?>
</div>
<?php $content = ob_get_clean(); ?>