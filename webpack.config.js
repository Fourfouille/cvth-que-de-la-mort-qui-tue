// Récupérer le module NodeJS "path", qui permet la résolution des liens d'un projet
const path = require("path")
// Récupération du module Mini CSS Extract Plugin, qui permet d'extraire le CSS compilé dans le JavaScript
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
    /**
     * EntryPoint
     * C'est ici que l'on définit la liste des fichiers qui doivent être
     * prit en compte pour la compilation avec Webpack
     */
    entry: {
        main: './assets/js/app.js'
    },

    /**
     * Output
     * Définit l'endroit et le nom des fichiers qui seront compilés depuis l'EntryPoint
     */
    output: {
        path: path.resolve(__dirname, 'public/dist'),
        filename: '[name].js'
    },

    /**
     * Modules
     * C'est ici qu'on définit la liste des règles de fonctionnement des modules
     * que l'on installe sur notre projet
     */
    module: {
        rules: [
            /**
             * Règle pour définir la compilation du Javascript
             * Ici, on va prendre tous les fichiers Javascript de notre
             * projet (sauf ceux des node_modules).
             * Et on va les compiler avec le module babel-loader.
             * Il se chargera de rendre notre javascript compatible
             * avec les anciennes versions de navigateur
             */ 
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },

            /**
             * Règle pour définir la compilation du SASS et/ou du CSS
             * Ici, on va prendre tous les fichiers .sass, .scss ou .css
             * Et on va les compiler avec les modules css-loader et
             * sass-loader.
             * On charge également PostCSSLoader qui permet d'ajouter les préfix CSS.
             * On charge resolve-url-loader pour résoudre les chemins relatives dans les
             * fichiers CSS.
             * Le MiniCssExtractPlugin va permettre d'extraire du javascript 
             * le css compilé en fichier css séparé.
             */
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    { 
                        loader: "css-loader",
                        options: {
                            importLoaders: 1
                        }
                    },          
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                            config: {
                                ctx: {
                                    cssnano: {},
                                    autoprefixer: {}
                                }
                            }
                        }
                    },
                    {
                        loader: 'resolve-url-loader'
                    },
                    { 
                        loader: "sass-loader",
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },

            /**
             * Règle pour définir la compilation des fonts
             * Ici, on va prendre tous les fichiers liés aux fonts.
             * Et on va les compiler avec le module file-loader.
             * Et on les compilera dans le dossier "fonts" situé
             * dans "public/dist"
             */
            {
                test: /\.(woff|woff2|ttf|otf|eot)$/,
                loader: 'file-loader',
                options: {
                    outputPath: 'fonts'
                }
            },

            /**
             * Règle pour définir la compilation des images
             * Ici, on va prendre tous les fichiers liés aux images.
             * Et on va les compiler avec le module file-loader.
             * Et on les compilera dans le dossier "img" situé
             * dans "public/dist".
             * On va également compresser nos images avec le module
             * image-webpack-loader (ce qui permet d'optimiser le poids des fichiers).
             */
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    { 
                        loader: 'file-loader',
                        options: {
                            outputPath: 'img'
                        }
                    },
                    { loader: 'image-webpack-loader' }
                ]
            }
        ]
    },

    /**
     * Chargement du plugin pour extraire le CSS du JavaScript
     */
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css",
            mode: "development",
        })
    ]
    
    
}; 